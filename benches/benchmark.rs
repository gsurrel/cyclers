#![feature(test)]

extern crate test; // Import the 'test' crate

use std::time::{Duration, Instant};

// Import the 'Bencher' trait from the 'test' crate
use test::Bencher;

// Import the 'libcyclers' library to benchmark
use libcyclers::{scheduler, timer};

// Define a benchmark test function
#[bench]
fn bench_schedule(bencher: &mut Bencher) {
    // Measure the time taken by the 'add' function
    bencher.iter(|| {
        let time_zero: Instant = Instant::now();

        let mut tasks = vec![
            scheduler::Task::new(time_zero, Duration::from_secs(1)),
            scheduler::Task::new(time_zero, Duration::from_secs(5)),
            scheduler::Task::new(time_zero, Duration::from_secs(5)),
            scheduler::Task::new(time_zero, Duration::from_secs(10)),
            scheduler::Task::new(time_zero, Duration::from_secs(10)),
        ];

        let max_concurrent_tasks = 2;

        let result = timer::run_tasks(
            &mut tasks,
            max_concurrent_tasks,
            timer::TimingMode::Simulation,
            Duration::from_secs(19),
            timer::Verbose::Off,
        )
        .unwrap();
        test::black_box(result); // Ensure the result is used
    });
}
