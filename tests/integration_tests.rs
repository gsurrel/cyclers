#[cfg(test)]
mod tests {
    use libcyclers::scheduler;
    use libcyclers::timer;
    use std::time::{Duration, Instant};

    #[test]
    fn test_exercice_from_scheduler() {
        let time_zero: Instant = Instant::now();

        let mut tasks = vec![
            scheduler::Task::new(time_zero, Duration::from_secs(1)),
            scheduler::Task::new(time_zero, Duration::from_secs(5)),
            scheduler::Task::new(time_zero, Duration::from_secs(5)),
            scheduler::Task::new(time_zero, Duration::from_secs(10)),
            scheduler::Task::new(time_zero, Duration::from_secs(10)),
        ];

        let max_concurrent_tasks = 2;
        let scheduler_interval = Duration::from_secs(1);

        let mut results: Vec<bool> = vec![];

        for i in 0..19 {
            println!("Running step time {}", i);
            let step = scheduler::run_next_tasks(
                &mut tasks,
                max_concurrent_tasks,
                time_zero + scheduler_interval * i,
            );
            results.append(&mut step.map(|res| res.1).unwrap());
        }

        #[rustfmt::skip]
        let expected = vec![
            /* 1 */  /* This line doesn't exist in this implementation as tasks A and B can be scheduled immediately */
            /* 2 */  true, true, false, false, false,
            /* 3 */  true, false, true, false, false,
            /* 4 */  true, false, false, true, false,
            /* 5 */  true, false, false, false, true,
            /* 6 */  true, false, false, false, false,
            /* 7 */  true, true, false, false, false,
            /* 8 */  true, false, true, false, false,
            /* 9 */  true, false, false, false, false,
            /* 10 */ true, false, false, false, false,
            /* 11 */ true, false, false, false, false,
            /* 12 */ true, true, false, false, false,
            /* 13 */ true, false, true, false, false,
            /* 14 */ true, false, false, true, false,
            /* 15 */ true, false, false, false, true,
            /* 16 */ true, false, false, false, false,
            /* 17 */ true, true, false, false, false,
            /* 18 */ true, false, true, false, false,
            /* 19 */ true, false, false, false, false,
            /* 20 */ true, false, false, false, false,
        ];
        assert_eq!(results, expected)
    }

    #[test]
    fn test_exercice_from_simulated_timer() {
        let time_zero: Instant = Instant::now();

        let mut tasks = vec![
            scheduler::Task::new(time_zero, Duration::from_secs(1)),
            scheduler::Task::new(time_zero, Duration::from_secs(5)),
            scheduler::Task::new(time_zero, Duration::from_secs(5)),
            scheduler::Task::new(time_zero, Duration::from_secs(10)),
            scheduler::Task::new(time_zero, Duration::from_secs(10)),
        ];

        let max_concurrent_tasks = 2;

        let result = timer::run_tasks(
            &mut tasks,
            max_concurrent_tasks,
            timer::TimingMode::Simulation,
            Duration::from_secs(19),
            timer::Verbose::Off,
        )
        .unwrap();

        println!("{}", result);

        let expected = "Time (sec) -> Tasks
1 -> A B
2 -> A C
3 -> A D
4 -> A E
5 -> A
6 -> A B
7 -> A C
8 -> A
9 -> A
10 -> A
11 -> A B
12 -> A C
13 -> A D
14 -> A E
15 -> A
16 -> A B
17 -> A C
18 -> A
19 -> A
";
        assert_eq!(result, expected)
    }

    #[test]
    fn test_exercice_from_simulated_timer_overconstrained() {
        let time_zero: Instant = Instant::now();

        let mut tasks = vec![
            scheduler::Task::new(time_zero, Duration::from_secs(1)),
            scheduler::Task::new(time_zero, Duration::from_secs(1)),
        ];

        let max_concurrent_tasks = 1;

        let result = timer::run_tasks(
            &mut tasks,
            max_concurrent_tasks,
            timer::TimingMode::Simulation,
            Duration::from_secs(19),
            timer::Verbose::Off,
        );

        let expected = Err("Some tasks are blocked because of unavailable resources!");
        assert_eq!(result, expected)
    }

    #[test]
    fn test_exercice_from_system_timer() {
        let time_zero: Instant = Instant::now();

        let mut tasks = vec![
            scheduler::Task::new(time_zero, Duration::from_secs(1)),
            scheduler::Task::new(time_zero, Duration::from_secs(5)),
            scheduler::Task::new(time_zero, Duration::from_secs(5)),
            scheduler::Task::new(time_zero, Duration::from_secs(10)),
            scheduler::Task::new(time_zero, Duration::from_secs(10)),
        ];

        let max_concurrent_tasks = 2;

        let result = timer::run_tasks(
            &mut tasks,
            max_concurrent_tasks,
            timer::TimingMode::Real,
            Duration::from_secs(19),
            timer::Verbose::Off,
        )
        .unwrap();

        println!("{}", result);

        let expected = "Time (sec) -> Tasks
1 -> A B
2 -> A C
3 -> A D
4 -> A E
5 -> A
6 -> A B
7 -> A C
8 -> A
9 -> A
10 -> A
11 -> A B
12 -> A C
13 -> A D
14 -> A E
15 -> A
16 -> A B
17 -> A C
18 -> A
19 -> A
";
        assert_eq!(result, expected)
    }
}
