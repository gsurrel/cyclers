# Cyclers

![Pipeline badge](https://gitlab.com/gsurrel/cyclers/badges/main/pipeline.svg) ![Code coverage badge](https://gitlab.com/gsurrel/cyclers/badges/main/coverage.svg)

This repository is a toy application (in Rust) scheduling short tasks running regularly in a cycle (at a different rate), while also showing good practices for project development.


## Building and running

Please check the [crate's documentation](https://gsurrel.gitlab.io/cyclers/doc/cyclers/) for information, which is also available in `src/main.rs`. Once the Rust toolchain is installed, you can compile and run writing `cargo run` in your shell. You can give arguments to the binary you run after a `--` if you start the binary using `cargo`.

```sh
cargo run -- --duration 19 --threads 2 --mode real 1 5 5 10 10
    Finished dev [unoptimized + debuginfo] target(s) in 0.03s
     Running `target/debug/cyclers --duration 19 --threads 2 --mode real 1 5 5 10 10`
Time (sec) -> Tasks
1 -> A B
2 -> A C
3 -> A D
4 -> A E
5 -> A
6 -> A B
7 -> A C
8 -> A
9 -> A
10 -> A
11 -> A B
12 -> A C
13 -> A D
14 -> A E
15 -> A
16 -> A B
17 -> A C
18 -> A
19 -> A
```

Help page of the tool:

```
A simple scheduling application to run tasks according to their rate and number of executors.

Usage: cyclers [OPTIONS] --duration <DURATION> --threads <THREADS> [TASK_RATES]...

Arguments:
  [TASK_RATES]...
          Get the list of task rates in seconds

Options:
  -d, --duration <DURATION>
          Scheduling duration in seconds

  -t, --threads <THREADS>
          Number of concurrent task threads

  -m, --mode <MODE>
          Use simulation mode

          [default: real]

          Possible values:
          - simulation: Simulation mode
          - real:       Real mode

  -h, --help
          Print help (see a summary with '-h')

  -V, --version
          Print version
```


***

## Programming language

This application is programmed in Rust, not only because of the merits of the language itself, but mostly because all the tooling that comes bundled with it, deeply connected with its mindset of correctness. For example, there is no additional tools required to perform the testing, generating the documentation, downloading the dependencies, etc.

As it can be seen in the pipeline definition file [`.gitlab-ci.yml`](https://gitlab.com/gsurrel/cyclers/-/blob/main/.gitlab-ci.yml), there is very little setup to perform to be able to have a full project configuration promoting good code quality.


## GitLab configuration

The source-code is [hosted on GitLab](https://gitlab.com/gsurrel/cyclers), with some light tuning of the default configuration:
- Branch protection rules for `main` to prevent pushing without going through a Merge Request (MR).
- Merging of branches into `main` should be limited to MRs that passed the code review.
- Commits are signed using the _SSH_ key, and should appear as _Verified_ on GitLab.
- GitLab's interactions are authenticated using the _SSH_ key, without any login/password.
- Merge request template text to help writing and checking that nothing is forgotten.


## Continuous Integration / Continuous Delivery (CI/CD)

A GitLab pipeline has been defined to ensure code quality during the development process:
- Check the code format (using the `rustfmt` tool).
- Check compiler and style warnings (using the `clippy` tool).
- Build the software.
- Run the tests and benchmarks.
- Generate and [publish the documentation](https://gsurrel.gitlab.io/cyclers/doc/cyclers/).
- Check the code-coverage to generate the GitLab badges.


## Releases

There is no dedicated and automated workflow for releases. It should be driven by CI/CD, but they only approach at this moment in time is to do the following:
- Update the changelog section adequately.
- Open a merge request to `main`.
- Tag the merge commit with the version number.
There is no additional step (such as shipping binaries).


## Dependencies

This project has been created with a single dependency: [`clap`](https://crates.io/crates/clap). It is used for parsing the command-line arguments. It is a well established and trusted library. In a more critical project, dependencies should be verified, and [`cargo-audit`](https://crates.io/crates/cargo-audit) can help for this task.

***

## Changelog

All notable changes to this project will be documented in this section.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).


## [Unreleased]

### Added

### Changed

### Deprecated

### Removed

### Fixed

### Security


## [0.2.0]

### Changed
- Compatibility with rustc 1.63.


## [0.2.0]

### Added
- Implemented tasks and scheduling.
- Implemented simulated timing option.
- Implemented real timing option.


## [0.1.0]

This first release has a completely non-functional software: it is just the very first release with a fully configured repository, CI/CD, and documentation.
