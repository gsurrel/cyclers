//! # Scheduler timer
//!
//! This module provides functions for executing tasks in simulation mode and real mode,
//! while managing timing and error propagation.
//!
//! In simulation mode, tasks are executed based on a manual clock with `Instant` and `Duration`.
//! In real mode, tasks are executed at regular intervals, waiting for the remaining time before
//! the next second.
//!
//! # Examples
//!
//! ```rust
//! use std::time::{Duration, Instant};
//! use libcyclers::scheduler;
//! use libcyclers::timer;
//!
//! let start_time = Instant::now();
//! let mut tasks = vec![
//!     scheduler::Task::new(start_time, Duration::from_secs(2)),
//!     scheduler::Task::new(start_time, Duration::from_secs(3)),
//! ];
//!
//! // Simulation mode
//! let duration = Duration::from_secs(5);
//! match timer::run_tasks(&mut tasks, 2, timer::TimingMode::Simulation, duration, timer::Verbose::Off) {
//!     Ok(_) => println!("Tasks executed in simulation mode."),
//!     Err(_) => println!("Error in simulation mode."),
//! }
//!
//! // Real mode
//! let duration = Duration::from_secs(5);
//! match timer::run_tasks(&mut tasks, 2, timer::TimingMode::Real, duration, timer::Verbose::Off) {
//!     Ok(_) => println!("Tasks executed in real mode."),
//!     Err(_) => println!("Error in real mode."),
//! }
//! ```

use crate::scheduler;
use std::time::{Duration, Instant};

/// Specifies the if output should be written or hidden.
#[derive(PartialEq)]
pub enum Verbose {
    /// Print to stdout
    On,
    /// Do not print anything
    Off,
}

/// Specifies the timing mode for task execution.
#[derive(Clone, clap::ValueEnum)]
pub enum TimingMode {
    /// Simulation mode
    Simulation,
    /// Real mode
    Real,
}

/// Executes tasks based on the provided timing mode and information.
///
/// Tasks are executed based on the specified `timing_mode`, which can be either
/// `TimingMode::Simulation(current_time)` or `TimingMode::Real(max_duration)`.
///
/// - **Real mode:** Tasks are executed at regular intervals, waiting for the remaining
/// time before the next second. The scheduling is as precise as the system threading
/// provides, but the tasks are updated with the theoretical wakeup time, to avoid
/// accumulating drift.
///
/// - **Simulated mode:** There is no delay in execution, all timings specified are
/// exact.
///
/// # Arguments
///
/// - `tasks`: A mutable reference to a vector of tasks to be scheduled and executed.
/// - `max_concurrent_tasks`: The maximum number of tasks that can run concurrently.
/// - `timing_mode`: The timing mode for task execution, either simulation or real.
/// - `duration`: The duration for which to schedule tasks.
/// - `verbose`: Whether to output text to the standard output or not.
///
/// # Returns
///
/// - `Ok(String)` if all eligible tasks are successfully scheduled and executed.
/// - `Err(&str)` if the task scheduling failed.
pub fn run_tasks(
    mut tasks: &mut [scheduler::Task],
    max_concurrent_tasks: usize,
    timing_mode: TimingMode,
    duration: Duration,
    verbose: Verbose,
) -> Result<String, &'static str> {
    let one_second = Duration::from_secs(1);
    let start_time = Instant::now();
    let mut iterations = 0;

    let mut result = String::from("Time (sec) -> Tasks\n");
    if verbose == Verbose::On {
        print!("{}", result);
    }

    let mut total_elapsed = Instant::now() - start_time;
    let mut scheduled_execution = start_time + iterations * one_second;
    while total_elapsed < duration {
        // Schedule the tasks
        let executed_tasks;
        (tasks, executed_tasks) =
            scheduler::run_next_tasks(tasks, max_concurrent_tasks, scheduled_execution)?;

        // Text result handling
        let time = total_elapsed.as_secs() + 1;
        let tasks_run = get_string_of_activated_tasks(&executed_tasks);
        let new_line = format!("{time} -> {tasks_run}\n");
        result.push_str(&new_line);
        if verbose == Verbose::On {
            print!("{}", new_line);
        }

        // Log iteration number
        iterations += 1;

        // Update the total elapsed time
        total_elapsed = match timing_mode {
            TimingMode::Simulation => {
                // Configure until the next trigger
                scheduled_execution = start_time + iterations * one_second;

                scheduled_execution - start_time
            }
            TimingMode::Real => {
                // Sleep until the next trigger
                scheduled_execution = start_time + iterations * one_second;
                let remaining_time = scheduled_execution - Instant::now();
                assert!(remaining_time.as_secs_f64() >= 0.);
                std::thread::sleep(remaining_time);

                Instant::now() - start_time
            }
        }
    }

    Ok(result)
}

/// Converts a slice of boolean values representing activated tasks into a string of task labels.
///
/// Given a slice `activated_tasks` containing boolean values indicating whether each task is activated,
/// this function generates a string where each activated task is represented by a corresponding letter label.
/// The letter labels are assigned in alphabetical order, starting from 'A' for the first activated task.
///
/// # Arguments
///
/// * `activated_tasks`: A reference to a slice of boolean values representing the activation status of tasks.
///
/// # Returns
///
/// A `String` containing task labels for the activated tasks, in the order they appear in the slice.
///
/// # Examples
///
/// ```
/// use libcyclers::timer::get_string_of_activated_tasks;
///
/// let activated_tasks = [true, false, true, false, true];
/// let result = get_string_of_activated_tasks(&activated_tasks);
/// assert_eq!(result, "A C E");
/// ```
///
/// In this example, `activated_tasks` contains five boolean values, and only tasks at positions 0, 2, and 4
/// are activated (true). The resulting string "A C E" corresponds to the activated tasks 'A', 'C', and 'E'.
///
/// ```
/// use libcyclers::timer::get_string_of_activated_tasks;
///
/// let activated_tasks = [false, false, false];
/// let result = get_string_of_activated_tasks(&activated_tasks);
/// assert_eq!(result, "");
/// ```
///
/// In this example, all tasks are deactivated (false), resulting in an empty string.
///
/// ```
/// use libcyclers::timer::get_string_of_activated_tasks;
///
/// let activated_tasks: [bool; 0] = [];
/// let result = get_string_of_activated_tasks(&activated_tasks);
/// assert_eq!(result, "");
/// ```
///
/// When the `activated_tasks` slice is empty, the function returns an empty string.
///
/// ```
/// use libcyclers::timer::get_string_of_activated_tasks;
///
/// let activated_tasks = [true, true, true];
/// let result = get_string_of_activated_tasks(&activated_tasks);
/// assert_eq!(result, "A B C");
/// ```
///
/// In this example, all tasks are activated, resulting in the string "A B C" with labels for tasks 'A', 'B', and 'C'.
///
/// ```
/// use libcyclers::timer::get_string_of_activated_tasks;
///
/// let activated_tasks = [true, false, true];
/// let result = get_string_of_activated_tasks(&activated_tasks);
/// assert_eq!(result, "A C");
/// ```
///
/// Here, tasks 'A' and 'C' are activated, and the resulting string contains labels for these tasks.
///
pub fn get_string_of_activated_tasks(activated_tasks: &[bool]) -> String {
    let mut result = String::new();

    for (index, &activated) in activated_tasks.iter().enumerate() {
        if activated {
            // Append a letter corresponding to the task to the result string
            let task_letter = char::from(b'A' + (index as u8));
            result.push(task_letter);

            // Add a space
            result.push(' ');
        }
    }

    // Pop the last char (a space) if there was an activated task
    _ = result.pop();

    result
}
