//! # Cyclers
//!
//! `cyclers` is a Rust command-line application that does very basic task scheduling,
//! as a pretext to show a good template project layout with the following elements:
//! - Binary and library separation of the code logic
//! - Unit tests
//! - Integration tests
//! - Documentation tests
//! - Documentation generation
//! - Benchmarks (using an unstable `bench` feature, only available in _nightly_ and not _stable_)
//! - Continuous integration
//! - Code coverage (source-based, rather than from instrumentation)
//! It takes user input regarding the tasks scheduling, and writes out the resulting scheduling in the terminal.
//! The scheduling happen every second, and tasks are supposed to complete under one second.
//!
//! There are no dedicated examples provided, as the library is used by the compiled binary and can be an example
//! by itself.
//!
//! ## Building and running
//!
//! Building this application only requires Rust. It has been developped again version 1.72 of the compiler,
//! which is the current one at this moment. Please check your compagny's policy regaring the installation
//! procedure of the Rust toolchain. The easiest way is to follow the instructions on <https://rustup.rs/>,
//! but you may prefer to install it using your favorite package manager (eg. `apt` or `brew`).
//!
//! All the commands are run in the directory containing the `Cargo.toml` file, root of the project. All the build
//! artifacts (debug/release/docuentation) are generated in the `target` directory by default.
//!
//! - *Building:* `cargo build` (use the `--release` flag to build an optimized binary)
//! - *Building and running:* `cargo run`
//!
//! ## Usage
//!
//! ```sh
//! cyclers <inputs>
//! ```
//! - `<inputs>`: A list of positive integers defining the execution rate of each task.
//!
//! ## Examples
//!
//! ```sh
//! # Run the binary with an input of 5 tasks, with one running every 1 second, two every 5
//! seconds, and two every 10 seconds.
//! cyclers 1 5 5 10 10
//!
//! # Output:
//! 1 -> A B
//! 2 -> A C
//! 3 -> A D
//! 4 -> A E
//! 5 -> A
//! 6 -> A B
//! 7 -> A C
//! 8 -> A
//! [...]
//! 18 -> A
//! 19 -> A
//! 20 -> A B
//! ```
//!
//! ## Author
//!
//! - Name: G. Surrel
//! - Profile: <https://gitlab.com/gsurrel/>
//!
//! ## License
//!
//! This project is licensed under the MIT License.
//!
//! ```
//! MIT License
//!
//! Copyright (c) <year> <copyright holders>
//!
//! Permission is hereby granted, free of charge, to any person obtaining a copy
//! of this software and associated documentation files (the "Software"), to deal
//! in the Software without restriction, including without limitation the rights
//! to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//! copies of the Software, and to permit persons to whom the Software is
//! furnished to do so, subject to the following conditions:
//!
//! The above copyright notice and this permission notice shall be included in all
//! copies or substantial portions of the Software.
//!
//! THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//! IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//! FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//! AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//! LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//! OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//! SOFTWARE.
//! ```
//!
//! For more details, please refer to the [Git repository](https://gitlab.com/gsurrel/cyclers).

use clap::Parser;
use libcyclers::{scheduler, timer};

#[derive(Parser)]
#[clap(author = "Grégoire Surrel")]
#[clap(version)]
#[clap(
    about = "A simple scheduling application to run tasks according to their rate and number of executors."
)]
#[clap(long_about = None)]
struct Args {
    /// Scheduling duration in seconds
    #[clap(short, long)]
    duration: u64,

    /// Number of concurrent task threads
    #[clap(short, long)]
    threads: usize,

    /// Use simulation mode
    #[clap(short, long, value_enum, default_value_t = libcyclers::timer::TimingMode::Real)]
    mode: libcyclers::timer::TimingMode,

    /// Get the list of task rates in seconds
    task_rates: Vec<u64>,
}

/// # Main Function
///
/// The `main` function is the entry point of the application. It is
/// currently minimalistic, as an initial setup before further expansion.
///
/// ## Panics
///
/// This `main` function does not panic under normal circumstances.
///
/// ## Errors
///
/// There are no specific error conditions for this `main` function.
fn main() -> Result<(), Box<dyn std::error::Error>> {
    let args = Args::parse();

    let time_zero = std::time::Instant::now();

    let mut tasks = vec![];
    for task_rate in args.task_rates {
        tasks.push(scheduler::Task::new(
            time_zero,
            std::time::Duration::from_secs(task_rate),
        ))
    }

    timer::run_tasks(
        &mut tasks,
        args.threads,
        args.mode,
        std::time::Duration::from_secs(args.duration),
        timer::Verbose::On,
    )
    .map(|_| ())
    .map_err(|err| err.into())
}
