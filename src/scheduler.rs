//! # Task scheduler
//!
//! This modules contains everything related to the task definition and scheduling.
//!
//! The design choice is to expose all the parameters regarding the current time where
//! relevant, in order to have the fexibility and control we may need (especially for
//! testing, where it becomes possible to have full control over the timing conditions).
//! This is instead of the scheduler and tasks reading directly the std::time::Instant
//! timer.
//!
//! The clock used to read the time is std::time::Instant, which is an opaque representation
//! of the current time, without any way to convert it to wall time. This is by design, as
//! wall time is not monotonic (lay-light saving time changes, time-zone changes, NTP clock
//! synchronizeation, etc).
//!
//! Finally, the design choses to leave the validity of the execution of a task to the
//! scheduler, rather than the tasks themsleves. This allows for possible other scheduling
//! strategies. For example, there could be a context (not supported in this project) where
//! a task needs to be forcefully run before its normal schedule. With this design, the task
//! will not complain about it.

use std::time::{Duration, Instant};

/// Ths struct represents a task that can be executed periodically. It is used to track
/// the last time the task was executed (`last_run_time`) and to specify the execution
/// rate (`rate`) at which the task should be performed. Internally, it tracks when it would
/// become late (`not_after`)
#[derive(Debug, Clone, PartialEq, Eq)]
pub struct Task {
    /// An optional `Instant` representing the timestamp of the last execution of the task.
    ///
    /// ⚠️ Tracking only the last time the task was run will lead to some drift in time
    /// regarding the trigger time (eg. with a schedule of 2s: 0, 2.01, 4.01, 6.02, etc)
    /// if no precaution is taken to mitigate that (eg. round to the closest second).
    pub last_run_time: Option<Instant>,

    /// An `Instant` tracking when the task would be late for execution.
    pub not_after: Instant,

    /// A `Duration` specifying the desired time interval between task executions.
    pub rate: Duration,
}

impl Task {
    /// Creates a new `Task` with the given identifier and execution rate.
    ///
    /// # Arguments
    ///
    /// - `rate`: A `Duration` specifying the desired time interval between task executions.
    ///
    /// # Returns
    ///
    /// A new `Task` instance with the provided identifier and execution rate. The `last_run_time`
    /// field is initially set to `None` to indicate that the task has not been executed yet.
    ///
    /// # Example
    ///
    /// ```rust
    /// use std::time::{Duration, Instant};
    /// use libcyclers::scheduler::Task;
    ///
    /// let task = Task::new(Instant::now(), Duration::from_secs(2));
    /// assert_eq!(task.last_run_time, None);
    /// assert_eq!(task.not_after > Instant::now(), true);
    /// ```
    pub fn new(now: Instant, rate: Duration) -> Self {
        Task {
            last_run_time: None,
            not_after: now + rate,
            rate,
        }
    }

    /// Executes the task and updates its `last_run_time`.
    ///
    /// This method simulates the execution of the task and updates the `last_run_time`
    /// field to the current time after execution.
    ///
    /// # Example
    ///
    /// ```rust
    /// use std::time::{Duration, Instant};
    /// use libcyclers::scheduler::Task;
    ///
    /// let now = Instant::now();
    /// let later = now + Duration::from_secs(1);
    /// let mut task = Task::new(now, Duration::from_secs(2));
    /// task.execute(later);
    /// assert_eq!(task.last_run_time, Some(later));
    /// assert_eq!(task.not_after, later + task.rate)
    /// ```
    pub fn execute(&mut self, current_time: Instant) {
        // Simulate task execution
        // Update the struct fields after execution
        self.last_run_time = Some(current_time);
        self.not_after = current_time + self.rate;
    }
}

/// Attempts to run the next set of tasks based on resource availability and scheduling deadlines.
///
/// This function schedules and runs tasks from the provided `tasks` vector based on resource contention
/// and scheduling deadlines. It ensures that tasks are executed periodically according to their specified
/// rates, and it respects the `max_concurrent_tasks` constraint to prevent resource exhaustion.
///
/// # Arguments
///
/// - `tasks`: A mutable reference to a vector of tasks that need to be scheduled and executed.
/// - `max_concurrent_tasks`: The maximum number of tasks that can run concurrently.
/// - `current_time`: The current `Instant` used to check task execution eligibility.
///
/// # Returns
///
/// - `Ok(Vec<bool>)` if all eligible tasks are successfully scheduled and executed. The vector of
///     booleans represents the tasks that were executed.
/// - `Err("Some tasks are late!")` if some tasks could not be scheduled due to resource contention.
/// - `Err("Some tasks are blocked because of unavailable resources!")` if some tasks should
///     have already been scheduled and executed, but they could not due to delay.
/// - `Err("Some tasks are late, some tasks are blocked!")` if the two situations above happen at once.
///
/// # Example
///
/// ```rust
/// use libcyclers::scheduler::*;
/// use std::time::{Duration, Instant};
///
/// let current_time = Instant::now();
///
/// let mut tasks = vec![
///     Task::new(current_time, Duration::from_secs(2)),
///     Task::new(current_time, Duration::from_secs(3)),
///     Task::new(current_time, Duration::from_secs(4)),
/// ];
///
/// let max_concurrent_tasks = 2;
///
/// match run_next_tasks(&mut tasks, max_concurrent_tasks, current_time) {
///     Ok(executed_tasks) => println!("Tasks scheduled and executed successfully."),
///     Err("Resource contention") => println!("Unable to schedule tasks due to resource contention."),
///     Err("Task is late") => println!("A task should have already been executed but is delayed."),
///     _ => (),
/// }
/// ```
///
/// In this example, the function `run_next_tasks` is used to schedule and execute tasks based on resource
/// availability and scheduling deadlines. It returns `Ok(())` if all eligible tasks are successfully executed,
/// or `Err` with a specific error message if scheduling constraints are violated.
pub fn run_next_tasks(
    tasks: &mut [Task],
    max_concurrent_tasks: usize,
    current_time: Instant,
) -> Result<(&mut [Task], Vec<bool>), &'static str> {
    let mut running_tasks = 0;

    // Store the number of late and blocked tasks
    let mut late_tasks = 0;
    let mut blocked_tasks = 0;

    // Store which tasks were scheduled
    let mut tasks_executed: Vec<bool> = vec![];

    // Iterate over the vector of tasks to run them when possible
    for task in tasks.iter_mut() {
        // Task should skip execution if it has a `last_run_time` and
        // the current time is less than the expected next execution
        let task_should_skip_execution = match task.last_run_time {
            None => false,
            Some(time) => current_time < time + task.rate,
        };
        let task_was_run = if !task_should_skip_execution {
            // The task can be run but it is late by more than 100ms
            if current_time > task.not_after + Duration::from_millis(100) {
                late_tasks += 1;
            }

            // The task cannot be run because all resources are used
            if running_tasks >= max_concurrent_tasks {
                if current_time >= task.not_after {
                    // The task is blocked
                    blocked_tasks += 1;
                }

                false
            } else {
                task.execute(current_time);
                running_tasks += 1;

                true
            }
        } else {
            false
        };

        tasks_executed.push(task_was_run);
    }

    if late_tasks > 0 && blocked_tasks > 0 {
        Err("Some tasks are late, some tasks are blocked!")
    } else if late_tasks > 0 {
        Err("Some tasks are late!")
    } else if blocked_tasks > 0 {
        Err("Some tasks are blocked because of unavailable resources!")
    } else {
        Ok((tasks, tasks_executed))
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_task_execute() {
        let time_zero = Instant::now();
        let rate = Duration::from_secs(2);
        let mut task = Task::new(time_zero, rate);

        // Initial configuration
        assert_eq!(task.last_run_time, None);
        assert_eq!(task.not_after, time_zero + rate);

        // Running it once, right now, only last_run_time should change
        task.execute(time_zero);
        assert_eq!(task.last_run_time, Some(time_zero));
        assert_eq!(task.not_after, time_zero + rate);

        // Running it once more, right now using the same time, nothing should change
        task.execute(time_zero);
        assert_eq!(task.last_run_time, Some(time_zero));
        assert_eq!(task.not_after, time_zero + rate);

        // Running it after two seconds, both last_run_time and not_after should update
        task.execute(time_zero + rate);
        assert_eq!(task.last_run_time, Some(time_zero + rate));
        assert_eq!(task.not_after, time_zero + rate + rate);
    }

    #[test]
    fn test_schedule_one_task_of_two_sec_on_one_executor() {
        let time_zero = Instant::now();
        let rate = Duration::from_secs(2);

        let mut tasks = vec![Task::new(time_zero, rate)];

        let max_concurrent_tasks = 1;

        // First steps, must work
        for i in 0..3 {
            // This step should run the task
            let time = 2 * i * Duration::from_secs(1);
            println!("Running step time {:?}", time);
            let execution_time = time_zero + time;
            let step = run_next_tasks(&mut tasks, max_concurrent_tasks, execution_time);
            assert_eq!(step.map(|res| res.1), Ok(vec![true]));

            // Check the task validity of the values
            let task_ref = tasks.get(0).unwrap().clone();
            assert_eq!(task_ref.last_run_time, Some(execution_time));
            assert_eq!(task_ref.not_after, execution_time + rate);

            // This step should not change the task at all
            let time = (2 * i + 1) * Duration::from_secs(1);
            println!("Running step time {:?}", time);
            let not_execution_time = time_zero + time;
            let step = run_next_tasks(&mut tasks, max_concurrent_tasks, not_execution_time);
            assert_eq!(step.map(|res| res.1), Ok(vec![false]));

            // Check the task validity of the values (the same as before as it must not change)
            let task = tasks.get(0).unwrap();
            assert_eq!(task, &task_ref);
        }
    }

    #[test]
    fn test_schedule_one_task_of_one_sec_on_one_executor() {
        let time_zero = Instant::now();
        let rate = Duration::from_secs(1);

        let mut tasks = vec![Task::new(time_zero, rate)];

        let max_concurrent_tasks = 1;

        // First steps, must work
        for i in 0..3 {
            println!("Running step time {}", i);
            let step = run_next_tasks(&mut tasks, max_concurrent_tasks, time_zero + rate * i);
            assert_eq!(step.map(|res| res.1), Ok(vec![true]));
        }

        // Jump to a late step
        println!("Running step time 7");
        let step = run_next_tasks(&mut tasks, max_concurrent_tasks, time_zero + rate * 7);
        assert_eq!(step.map(|res| res.1), Err("Some tasks are late!"));
    }

    #[test]
    fn test_schedule_two_task_of_one_sec_on_one_executor() {
        let time_zero = Instant::now();
        let rate = Duration::from_secs(1);

        let mut tasks = vec![Task::new(time_zero, rate), Task::new(time_zero, rate)];

        let max_concurrent_tasks = 1;

        // First step pass: one task can start immediately, the other one can wait until a full second
        // has passed (at which point both tasks *need* to run)
        let step = run_next_tasks(&mut tasks, max_concurrent_tasks, time_zero);
        assert_eq!(step.map(|res| res.1), Ok(vec![true, false]));

        let step = run_next_tasks(&mut tasks, max_concurrent_tasks, time_zero + rate);
        assert_eq!(
            step,
            Err("Some tasks are blocked because of unavailable resources!")
        );
    }

    #[test]
    fn test_schedule_two_task_of_three_and_two_sec_on_one_executor() {
        let time_zero = Instant::now();

        let mut tasks = vec![
            Task::new(time_zero, Duration::from_secs(3)),
            Task::new(time_zero, Duration::from_secs(2)),
        ];

        let max_concurrent_tasks = 1;

        // First step (t=0), must pass: task 1 runs
        let step = run_next_tasks(&mut tasks, max_concurrent_tasks, time_zero);
        assert_eq!(step.map(|res| res.1), Ok(vec![true, false]));

        // Second step (t=1), must pass: task 2 runs
        let step = run_next_tasks(
            &mut tasks,
            max_concurrent_tasks,
            time_zero + Duration::from_secs(1),
        );
        assert_eq!(step.map(|res| res.1), Ok(vec![false, true]));

        // Third step (t=5), must fail: both should be running at once, and both are late
        let step = run_next_tasks(
            &mut tasks,
            max_concurrent_tasks,
            time_zero + Duration::from_secs(5),
        );
        assert_eq!(step, Err("Some tasks are late, some tasks are blocked!"));
    }

    #[test]
    fn test_schedule_two_task_of_one_and_two_sec_on_two_executors() {
        let time_zero = Instant::now();

        let mut tasks = vec![
            Task::new(time_zero, Duration::from_secs(1)),
            Task::new(time_zero, Duration::from_secs(2)),
        ];

        let max_concurrent_tasks = 2;

        // First step (t=0), must pass: both tasks run
        let step = run_next_tasks(&mut tasks, max_concurrent_tasks, time_zero);
        assert_eq!(step.map(|res| res.1), Ok(vec![true, true]));

        // Second step (t=1), must pass: task 1 runs
        let step = run_next_tasks(
            &mut tasks,
            max_concurrent_tasks,
            time_zero + Duration::from_secs(1),
        );
        assert_eq!(step.map(|res| res.1), Ok(vec![true, false]));

        // Third step (t=2), both tasks run
        let step = run_next_tasks(
            &mut tasks,
            max_concurrent_tasks,
            time_zero + Duration::from_secs(2),
        );
        assert_eq!(step.map(|res| res.1), Ok(vec![true, true]));
    }
}
